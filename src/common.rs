//! Common useful code needed repeatedly throughout the whole program aggregated
//! here in a convenient place.

use std::{error::Error, fmt::{Display, Formatter}};

use gst::ElementFactory;
use strum_macros::Display;

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Display, Clone, Copy)]
pub enum VideoCodec {
    VP9,
    AV1,
    AVC,
}

#[derive(Display, Clone, Copy)]
pub enum AudioCodec {
    OPUS,
    M4A,
}

pub struct Compatibility {
    pub acodec: AudioCodec,
    pub vcodec: VideoCodec,

    pub vp9: bool,
    pub av1: bool,
    pub avc: bool,

    pub opus: bool,
    pub m4a: bool,
}

impl Compatibility {
    pub fn new() -> std::result::Result<Self, MissingCodecError> {
        let mut vp9 = false;
        if ElementFactory::find("vp9dec").is_some() {
            vp9 = true;
        }

        let mut av1 = false;
        if ElementFactory::find("av1dec").is_some() {
            av1 = true;
        }

        let mut avc = false;
        if ElementFactory::find("avdec_mpeg4").is_some() {
            avc = true;
        }

        let mut opus = false;
        if ElementFactory::find("opusdec").is_some() {
            opus = true;
        }

        let mut m4a = false;
        if ElementFactory::find("avdec_aac").is_some() {
            m4a = true;
        }

        if !(vp9 || av1 || avc || opus || m4a) {
            return Err(MissingCodecError { video: true, audio: true });
        }

        let vcodec = if vp9 {
            VideoCodec::VP9
        } else if av1 {
            VideoCodec::AV1
        } else if avc {
            VideoCodec::AVC
        } else {
            return Err(MissingCodecError { video: true, audio: false });
        };

        let acodec = if opus {
            AudioCodec::OPUS
        } else if m4a {
            AudioCodec::M4A
        } else {
            return Err(MissingCodecError { video: false, audio: true });
        };

        Ok(Self { acodec, vcodec, vp9, av1, avc, opus, m4a })
    }
}

#[derive(Debug)]
pub struct MissingCodecError {
    pub audio: bool,
    pub video: bool,
}

impl Display for MissingCodecError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.audio && self.video {
            write!(f, "Neither audio (opus, m4a) nor video (vp9, av1, avc) codecs were found")
        } else if self.audio {
            write!(f, "No compatible audio codec (opus, m4a) was found")
        } else if self.video {
            write!(f, "No compatible video codec (vp9, av1, avc) was found")
        } else {
            write!(f, "No compatible codec was found")
        }
    }
}

impl Error for MissingCodecError {}