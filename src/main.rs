use gettextrs::*;
use gio::prelude::*;
use gtk::prelude::*;

mod config;
mod window;
use crate::window::Window;

fn main() {
    gst::init().unwrap_or_else(|_| panic!("Failed to initialize Gstreamer."));

    gtk::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("sharit", config::LOCALEDIR);
    textdomain("sharit");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/sharit.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    let app = gtk::Application::new(Some("eu.lyes.Sharit"), Default::default());
    app.connect_activate(move |app| {
        let window = Window::new();

        window.widget.set_application(Some(app));
        app.add_window(&window.widget);
        window.widget.present();
    });

    let ret = app.run();
    std::process::exit(ret);
}
