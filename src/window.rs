use sharit::backend::{video::Widget, youtube::video::YouTubeWidget};

use gtk::prelude::*;
//use libhandy::prelude::*;

pub struct Window {
    pub widget: adw::ApplicationWindow,
}

impl Window {
    pub fn new() -> Self {
        let builder = gtk::Builder::from_resource("/eu/lyes/Sharit/ui/window.ui");
        let widget: adw::ApplicationWindow = builder
            .object("window")
            .expect("Failed to find the window object");

        let main_box: gtk::Box = builder
            .object("main_box")
            .expect("Failed to find the box");

        let player = YouTubeWidget::new("BaW_jenozKc".to_owned()).unwrap();
        let player = player.get_widget().unwrap();

        main_box.append(&player);

        Self { widget }
    }
}
