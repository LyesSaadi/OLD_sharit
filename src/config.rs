#![allow(dead_code)]

pub static PKGDATADIR: &str = "/usr/local/share/sharit";
pub static VERSION: &str = "0.1.0";
pub static LOCALEDIR: &str = "/usr/local/share/locale";