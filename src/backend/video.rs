use crate::common::Result;

use std::time::Duration;

pub trait Widget: Sized {
    fn new(id: String) -> Result<Self>;
    fn get_widget(&self) -> Result<gtk::Widget>;
    fn play(&self);
    fn pause(&self);
    fn position(&self) -> Duration;
    fn cached(&self) -> Duration;
    fn seek(&self, position: Duration);
    fn loading(&self) -> bool;
}

pub trait Format: Sized {
}


/*
TODOs:
- [ ] Create basic player with fixed resolution.
- [ ] Adding buffering.
- [ ] Adding an overlay and appropriate signals.
- [ ] Adding subtitles.
- [ ] Adding the auto resolution.
*/
