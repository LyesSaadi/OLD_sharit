use std::{panic, error::Error, fmt::{Display, Formatter}, str::FromStr};

use crate::backend::video::Widget;
use crate::backend::youtube::video::YouTubeExtractor;

use super::{Format, SubtitleExtension};
use crate::common::{AudioCodec, Compatibility, Result, VideoCodec};

use gst::prelude::*;

pub struct YouTubeWidget {
    extractor: YouTubeExtractor,
}

impl Widget for YouTubeWidget {
    fn new(id: String) -> Result<Self> {
        let extractor = YouTubeExtractor::from_id(id)?;

        Ok(Self { extractor })
    }

    fn get_widget(&self) -> Result<gtk::Widget> {
        // Creating the Pipeline, Muxer and Sink.
        let pipeline = gst::Pipeline::new(None);
        let mux = gst::ElementFactory::make("matroskamux", None)?;
        let sink = gst::ElementFactory::make("gtk4glsink", None)?;

        // Getting the compatible codecs.
        let compat = Compatibility::new()?;

        let video = DecoderBin::new_video(
            &self.extractor,
            compat.vcodec,
            480,
            30
        )?;

        let audio = DecoderBin::new_audio(
            &self.extractor,
            compat.acodec,
            160
        )?;

        let sub = DecoderBin::new_subtitle(
            &self.extractor,
            false,
            "en",
            SubtitleExtension::TTML
        )?;

        // Adding everything to the pipeline.
        pipeline.add_many(&[&video.bin, &audio.bin, &sub.bin])?;
        pipeline.add_many(&[&mux, &sink])?;

        // Get all sink pads.
        //let video_sink_pad = mux.request_pad_simple("video_%u").unwrap();
        //let audio_sink_pad = mux.request_pad_simple("audio_%u").unwrap();
        //let sub_sink_pad = mux.request_pad_simple("subtitle_%u").unwrap();

        // Linking all the DecoderBin to the muxer.
        //video.pad.link(&video_sink_pad)?;
        //audio.pad.link(&audio_sink_pad)?;
        //sub.pad.link(&sub_sink_pad)?;

        // Linking the muxer to the sink.
        //mux.link(&sink).unwrap();
        video.bin.link(&sink)?;

        let widget = sink.property("widget")?
            .get::<gtk::Widget>()?;

        Ok(widget)
    }

    fn play(&self) {
        todo!()
    }

    fn pause(&self) {
        todo!()
    }

    fn position(&self) -> std::time::Duration {
        todo!()
    }

    fn cached(&self) -> std::time::Duration {
        todo!()
    }

    fn seek(&self, _position: std::time::Duration) {
        todo!()
    }

    fn loading(&self) -> bool {
        todo!()
    }
}

pub struct DecoderBin {
    pub bin: gst::Bin,
    pub src: gst::Element,
    pub buf: gst::Element,
    pub dec: gst::Element,
    pub pad: gst::GhostPad,
}

impl DecoderBin {
    pub fn new(loc: &str, dec_name: &str) -> Result<Self> {
        // Creating the Elements.
        let bin = gst::Bin::new(None);
        let src = gst::ElementFactory::make("souphttpsrc", None)?;
        let buf = gst::ElementFactory::make("downloadbuffer", None)?;
        let dec = gst::ElementFactory::make(dec_name, None)?;

        // Adding the Elements to the Bin.
        bin.add_many(&[&src, &buf, &dec])?;

        // Configuring the location of the stream to download.
        src.set_property("location", &loc.to_owned())?;

        // Configuring the buffer with a limit of 15min (900s, 9x10^11).
        buf.set_property("max-size-time", &900000000000u64)?;
        buf.set_property("max-size-bytes", &0u32)?;

        // Linking the Pads.
        gst::Element::link_many(&[&src, &buf, &dec])?;

        // Creating the ghost pad.
        let pad = gst::GhostPad::new(None, gst::PadDirection::Src);
        bin.add_pad(&pad)?;

        Ok(DecoderBin { bin, src, buf, dec, pad })
    }

    pub fn new_video(
        extractor: &YouTubeExtractor,
        codec: VideoCodec,
        res: usize,
        fps: usize,
    ) -> Result<Self> {
        let dec_name = match codec {
            VideoCodec::VP9 => "vp9dec",
            VideoCodec::AV1 => "av1dec",
            VideoCodec::AVC => "avdec_mpeg4",
        };

        let codec = format!("{}{}p{}", codec, res, fps);
        let format = Format::from_str(&codec)?;
        let streams = extractor.get_streams();
        let stream = match streams.get(&format) {
            Some(s) => s.clone(),
            None => {
                let err = MissingStreamError { stream: codec };
                return Err(Box::new(err));
            },
        };
        let loc = &stream.url.unwrap();

        Self::new(loc, dec_name)
    }

    pub fn new_audio(
        extractor: &YouTubeExtractor,
        codec: AudioCodec,
        quality: usize
    ) -> Result<Self> {
        let dec_name = match codec {
            AudioCodec::OPUS => "opusdec",
            AudioCodec::M4A => "avdec_aac",
        };

        let codec = format!("{}{}k", codec, quality);
        let format = Format::from_str(&codec)?;
        let streams = extractor.get_streams();
        let stream = match streams.get(&format) {
            Some(s) => s.clone(),
            None => {
                let err = MissingStreamError { stream: codec };
                return Err(Box::new(err));
            },
        };
        let loc = &stream.url.unwrap();

        Self::new(loc, dec_name)
    }

    /// # Panics
    /// This function panics if the extension is not supported. At the time of
    /// writing, only TTML is supported.
    pub fn new_subtitle(
        extractor: &YouTubeExtractor,
        auto: bool,
        lang: &str,
        ext: SubtitleExtension,
    ) -> Result<Self> {
        let dec_name = match ext {
            SubtitleExtension::TTML => "ttmlparse",
            _ => {
                panic!("Unsupported Subtitles");
            },
        };

        let subtitles = extractor.get_subtitles();
        let sub_lang = match subtitles[auto as usize].get(lang) {
            Some(s) => s,
            None => {
                let err = MissingStreamError {
                    stream: format!("\"{}\" subtitles", lang)
                };
                return Err(Box::new(err));
            },
        };
        let loc = match sub_lang.get(&ext) {
            Some(url) => url,
            None => {
                let err = MissingStreamError {
                    stream: format!("\"{}\" {} subtitles", lang, ext)
                };
                return Err(Box::new(err));
            },
        };

        Self::new(loc, dec_name)
    }
}

#[derive(Debug)]
pub struct MissingStreamError {
    pub stream: String,
}

impl Display for MissingStreamError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "There is no {} stream available for this video", self.stream)
    }
}

impl Error for MissingStreamError {}