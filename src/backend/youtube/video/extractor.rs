//! Module managing through youtube_dl the retrieval of all informations
//! concerning YouTube videos, their metadatas and their streams.

use pyo3::prelude::*;
use pyo3::types::{PyString, IntoPyDict};
use serde::Deserialize;

use std::collections::HashMap;

use crate::common::Result;
use super::*;

/// Structure which communicate with
/// [`youtube_dl`](https://github.com/ytdl-org/youtube-dl) to extract a YouTube
/// video's infos.
///
/// # Usage
/// ```
/// # use sharit::backend::youtube::video::YouTubeExtractor;
/// let mut v = YouTubeExtractor::new();
/// v.id = Some(String::from("BaW_jenozKc"));
/// v.reload();
/// ```
///
/// Or, use the from_id() shortcut function:
/// ```
/// # use sharit::backend::youtube::video::YouTubeExtractor;
/// let v = YouTubeExtractor::from_id(String::from("BaW_jenozKc"));
/// ```
///
/// # Panics
/// This _should not_ panics, but, it does have one call to `unwrap` for
/// ownership reasons. If it does indeed panic, that is a bug, and it should be
/// reported.
#[derive(Deserialize, Debug, Default)]
pub struct YouTubeExtractor {
    /// The video's identifier.
    pub id: Option<String>,
    /// The video's URL.
    #[serde(rename = "webpage_url")]
    pub url: Option<String>,

    /// The video's title.
    pub title: Option<String>,
    /// The video's description.
    pub description: Option<String>,
    /// The thumbnail's url.
    pub thumbnail: Option<String>,
    /// The video's duration.
    pub duration: Option<usize>,
    /// The upload date.
    pub upload_date: Option<String>,
    /// Is the video a live stream?
    pub is_live: Option<bool>,

    /// The uploader name.
    pub uploader: Option<String>,
    /// The uploader's identifier.
    pub uploader_id: Option<String>,
    /// The uploader's channel URL.
    pub uploader_url: Option<String>,

    /// The video's categories.
    pub categories: Option<Vec<String>>,
    /// The video's tags.
    pub tags: Option<Vec<String>>,

    /// The available streams from which to download the video.
    #[serde(rename = "formats")]
    pub streams: Option<Vec<Stream>>,

    // The available subtitles.
    pub subtitles: Option<HashMap<String, Vec<Subtitle>>>,
    // The available automatic captions.
    pub automatic_captions: Option<HashMap<String, Vec<Subtitle>>>,
}

impl YouTubeExtractor {
    /// Creates a new, empty, object.
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates a new object, specify an id, and reload the infos.
    pub fn from_id(id: String) -> Result<Self> {
        let mut v = Self::new();
        v.id = Some(id);
        v.reload()?;
        Ok(v)
    }

    /// Calls `youtube_dl` and extract all the information from the video and
    /// writes it in the same object.
    ///
    /// # Error
    /// This fails if `self.id` is `None`.
    pub fn reload(&mut self) -> Result<&mut Self> {
        // Creating a Python environment.
        let gil = Python::acquire_gil();
        let py = gil.python();

        // Importing `youtube_dl.YoutubeDL` and `json.dumps`. And calling
        // `YoutubeDL()`.
        let ydl = py.import("youtube_dl")?.get("YoutubeDL")?;
        let dumps = py.import("json")?.get("dumps")?;

        // Pass arguments to the `youtube_dl.YoutubeDL`.
        let args = vec![
            ("writesubtitles", true), // Include subtitles
            ("writeautomaticsub", true), // Include automatic captions
            ("quiet", true), // To avoid clutering the stdout with useless junk
            ("no_warnings", true), // Same for warnings
            ("simulate", true), // We do not want to download the video
        ].into_py_dict(py);
        let ydl = ydl.call1((args,))?;

        // Recreating the URL from the ID.
        let mut url = String::from("https://www.youtube.com/watch?v=");
        url += self.id.clone().unwrap_or_default().as_str();

        // Calling `YoutubeDL().extract_info(url)`.
        let info = ydl.call_method1("extract_info", (url,))?;

        // Serializing the output into JSON.
        let json = dumps.call1((info,))?;

        // Getting the JSON into a Rust String. For some reason, PyErr uses
        // the local variable `py`, which create an ownership problem, which
        // makes it impossible to just return the error. But, this should not
        // theoretically fail anyway. So we can `unwrap()` safely.
        let metadata = json.downcast::<PyString>().unwrap().to_str()?;

        // This is honestly awesome, we swap the old VideoExtractor with a new
        // one created from the returned JSON.
        *self = serde_json::from_str(metadata)?;

        Ok(self)
    }

    /// Returns a HashMap of Streams for more flexibility and easier searching.
    pub fn get_streams(&self) -> HashMap<Format, Stream> {
        let streams = self.streams.clone().unwrap_or_default();
        streams.iter()
            .map(|s| (s.format.unwrap(), s.clone()))
            .collect()
    }

    /// Returns two nested HashMap of urls indicating the language and the type.
    /// The first nested HashMap is for subtitles, the second for automatic
    /// captions, returned in this form: `HashMap<lang, HashMap<ext, url>>`.
    pub fn get_subtitles(&self)
    -> [HashMap<String, HashMap<SubtitleExtension, String>>; 2] {
        let extract = |(lang, subs): (&String, &Vec<Subtitle>)| {
            let sub = subs.iter()
                .map(|sub| (sub.ext, sub.url.clone()))
                .collect();
            (lang.clone(), sub)
        };

        let subtitles = self.subtitles.clone().unwrap_or_default();
        let subtitles = subtitles.iter()
            .map(extract)
            .collect();

        let auto_subs = self.automatic_captions.clone().unwrap_or_default();
        let auto_subs = auto_subs.iter()
            .map(extract)
            .collect();

        [subtitles, auto_subs]
    }
}
