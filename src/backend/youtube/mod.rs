pub mod video;

#[cfg(test)]
mod tests {
    use super::video::*;

    #[test]
    fn get_title() {
        let v = YouTubeExtractor::from_id("BaW_jenozKc".to_owned()).unwrap();

        assert_eq!("youtube-dl test video \"'/\\ä↭𝕐", &v.title.unwrap());
    } 

    #[test]
    fn get_stream() {
        let v = YouTubeExtractor::from_id("BaW_jenozKc".to_owned()).unwrap();
        let streams = v.get_streams();

        assert_eq!(Some(String::from("720p")),
            streams.get(&Format::ALL720p30).unwrap().resolution);

        assert_eq!(Some(Extension::MP4),
            streams.get(&Format::ALL720p30).unwrap().ext);

        assert_eq!(Some(String::from("22 - 1280x720 (720p)")),
            streams.get(&Format::ALL720p30).unwrap().format_name);
    }

    #[test]
    fn try_reload() {
        let mut v = YouTubeExtractor::from_id("BaW_jenozKc".to_owned()).unwrap();

        assert_eq!("youtube-dl test video \"'/\\ä↭𝕐",
            &v.title.clone().unwrap());

        v.id = Some(String::from("rAl-9HwD858"));
        v.reload().unwrap();

        assert_eq!("Crust of Rust: Lifetime Annotations",
            &v.title.unwrap());
    }
}